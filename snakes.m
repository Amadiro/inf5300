THRESHOLD = 120; % figure out best value for this
STDDEV_WINDOW_SIZE = 5;

load 'seismic_timeslice.mat';
myimage = tsl_input;


[width, height] = size(myimage);


%%
%% stddev-based filter + edge detection
%%

myimage(myimage >= THRESHOLD) = 1.0;
myimage(myimage < -THRESHOLD) = -1.0;

myimage_stddev = stdfilt(myimage, ones([STDDEV_WINDOW_SIZE STDDEV_WINDOW_SIZE]));

edges = dosobel(myimage_stddev, 0.45);
closed = imclose(edges, strel('disk', 3));

%% Now need a sobel without thresholding
filter = -fspecial('sobel');
vertical = imfilter(closed, filter, 'replicate');
horizontal = imfilter(closed, filter', 'replicate');

close all;

colormap 'gray'
imagesc(closed);
print('snake-input.png', '-dpng');

dosnake(vertical, horizontal, myimage, 50, 50, 0.0001, 0.08, 1.6, 0.2);
dogvfsnake(closed, myimage, 50, 80, 60, 0.00001, 0.01, 1.9, 0.1);

