function accuracy = svm_iteration(sigma, C, train_classes, train_data, test_classes, test_data)
  parameters = ['-t 2 -c ' num2str(C) ' -g ' num2str(sigma) ' -q'];
  model = svmtrain(train_classes, train_data, parameters);

  %[~ predicted_label] = evalc('svmpredict(Ylabel, Xlabel, model)');
  [out, predicted_labels, acc, prob_estimates] = evalc('svmpredict(test_classes, test_data, model)');
  accuracy = acc(1);
end
