function output_image = dosobel(input_image, thresh)
  THRESHOLD = thresh;
  % softer sobel than matlabs built-in, with adjustable threshold.
  filter = -fspecial('sobel');
  vertical = imfilter(input_image, filter, 'replicate');
  horizontal = imfilter(input_image, filter', 'replicate');

  output_image = sqrt(horizontal .* horizontal + vertical .* vertical);
  output_image = output_image/max(max(output_image));
  output_image(output_image >= THRESHOLD) = 1.0;
  output_image(output_image < THRESHOLD) = -1.0;
end
