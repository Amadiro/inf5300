function output_data = fisher(input_data, class_labels)
  class_averages = zeros(9, size(input_data, 2));
  for i = 1:9
    class_averages(i, 1:end) = mean(input_data(class_labels == i, 1:end), 1);
  end
  total_average = mean(class_averages, 1);

  shifted = class_averages;
  for i = 1:size(shifted, 1)
    shifted(i, 1:end) = class_averages(i, 1:end) - total_average; % mu_i - mu
  end
  
  S_b = shifted'*shifted;

  S_w = zeros(size(class_averages, 2), size(class_averages, 2));
  
  for i = 1:9
    % generate a shifted class
    class = input_data(class_labels == i, 1:end);
    shifted_class = class;
    for j = 1:size(class, 1)
      shifted_class(j, 1:end) = class(j, 1:end) - class_averages(i, 1:end);
    end
    
    % produce and sum the individual covariance matrices
    for j = 1:9
      S_w = S_w + shifted_class'*shifted_class;
    end
  end

  result_rank = rank(S_b); % The # of good eigenvectors we get. Rest is discarded
  res = S_w \ S_b;
  [U, D] = eig(res);
  D = diag(D);
  [sorted, index] = sort(D, 'descend');
  sorted_eigenvectors = U(1:end, index);

  sorted_eigenvectors = U(1:end, 1:result_rank);
  sorted_eigenvectors = sorted_eigenvectors';
  size(sorted_eigenvectors)
  

  % sorted eigenvectors has M-1 vectors as columns.
  input_data2 = input_data';
  size(input_data2)
  
  %output_data = zeros(result_rank, size(input_data, 2));

  %for i=1:size(input_data2, 2)
  %  output_data(1:end, i) = sorted_eigenvectors*input_data2(1:end, i);
  %end
  output_data = sorted_eigenvectors*(input_data2);
end
