granularity = 64000


Cs = [2^-1, 2^1, 2^3, 2^5, 2^7, 2^9, 2^11, 2^13];
sigmas = [2^-11, 2^-9, 2^-7, 2^-5, 2^-3, 2^-1, 2^1, 2^3];

				%Cs = linspace(2^-3, 2^13, granularity);
Cs = linspace(2^-13, 60, granularity);
sigmas = (1.0)./Cs;

accuracies = []
tic()
for i = 1:size(Cs, 2)
  disp(['Evaluating for C=' num2str(Cs(i)) ', sigma=' num2str(sigmas(i)) '(' num2str(100*i/size(Cs, 2)) '% done)']);
  acc = judge_svm_parameters(pavia_trainset, pavia_testset, sigmas(i), Cs(i));
  accuracies = [accuracies; acc];
  acc
end
format long g;
results = [accuracies Cs' sigmas']
[m, i] = max(accuracies);
disp(['max found at C=' num2str(Cs(i)) ', sigma=' num2str(sigmas(i)) ': ' num2str(accuracies(i))])  

plot(results(:, 2), results(:, 1));
axis([0 50 88 100])
									   print('accuracy.pdf', '-dpdf');
									   toc()