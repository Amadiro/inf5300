n = 500;

theta = pi/4;
rot = [cos(theta), -sin(theta); sin(theta), cos(theta)];

data = [randn(n,1)+ 5*rand(n,1), rand(n,1)]*rot;


[Y,d] = mypca(data);
figure();
plot(data(:,1), data(:,2), '*');
title('Original data');

print('pcatest-before.pdf', '-dpdf');

figure();
plot(Y(:,1), Y(:,2), '*');
title('after PCA');

print('pcatest-after.pdf', '-dpdf');
