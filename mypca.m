function [output_data, eigenvalues] = mypca(input_data)
  shifted_input_data = input_data;
  %size(input_data)

  for i = 1:size(input_data, 2)
    m = mean(input_data(1:end, i));
    shifted_input_data(1:end, i) = input_data(1:end, i) - m;
  end
    
  cov_matrix = zeros(size(input_data, 2), size(input_data, 2));
    
  %for i = 1:size(input_data, 1)
  %  cov_matrix = cov_matrix + input_data(i, 1:end)'*input_data(i, 1:end);
				%end
  %% TODO: eig values unsorted, need sorting!
  cov_matrix = shifted_input_data'*shifted_input_data;
  size(cov_matrix)
  [V, D] = eig(cov_matrix);
  V = fliplr(V);

  output_data = shifted_input_data*V;  
  eigenvalues = fliplr(diag(D));
end
