C=6.8893;
sigma=0.14515;
% accuracy from cross-training: 99.5122

train_classes = pavia_trainset.nlab(pavia_trainset.nlab == 4 | pavia_trainset.nlab == 5);
train_data = pavia_trainset.data(pavia_trainset.nlab == 4 | pavia_trainset.nlab == 5, :);

test_classes = pavia_testset.nlab(pavia_testset.nlab == 4 | pavia_testset.nlab == 5);
test_data = pavia_testset.data(pavia_testset.nlab == 4 | pavia_testset.nlab == 5, :);

max_train = max(train_data);
min_train = min(train_data);
for i=1:size(train_data)
  train_data(i,:) = 2*(train_data(i,:) - min_train)./(max_train - min_train) - 1;
end
for i=1:size(test_data)
  test_data(i,:) = 2*(test_data(i,:) - min_train)./(max_train-min_train) - 1;
end

accuracy = svm_iteration(sigma, C, train_classes, train_data, test_classes, test_data)