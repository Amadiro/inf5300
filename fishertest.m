
%% create random samples in circular region (not around origin)
%% chose some line like y = x
%% make all samples above line one class, samples below line another
%% run through fisher
%% plot so that it's clear which samples belong to which class

NUM_POINTS = 1000;

%% Generate points.
%% TODO: make then not be around origo

pts = [];
while size(pts, 1) < NUM_POINTS
  pt = [2.0*rand() - 1.0, 2.0*rand() - 1.0, 2.0*rand() - 1.0];
  if pt(1).^2.0 + pt(2).^2.0 + pt(3).^2.0 < 1.0
    pts = [pts; pt];
  end
end

%% Generate classes

class_assignment = [];
for i = 1:size(pts, 1)
  if pts(i, 1) < pts(i, 2) 
    class_assignment = [class_assignment; 1];
  else
    class_assignment = [class_assignment; 2];
  end
end

%% Plot with assignment
hold on
xs = pts(1:end, 1);
ys = pts(1:end, 2);
for i = 1:size(pts, 1)
  if class_assignment(i) == 1
    plot(xs(i, 1:end), ys(i, 1:end), 'x');
  else
    plot(xs(i, 1:end), ys(i, 1:end), 'o');
  end
end
hold off

newdata = fisher(pts, class_assignment);

%% Plot with assignment
hold on
xs = newdata(1:end, 1);
ys = newdata(1:end, 2);
for i = 1:size(pts, 1)
  if class_assignment(i) == 1
    plot(xs(i, 1:end), ys(i, 1:end), 'x');
  else
    plot(xs(i, 1:end), ys(i, 1:end), 'o');
  end
end
hold off

%data = [randn(n,1)+ 5*rand(n,1), rand(n,1)]*rot;

%[Y,d] = mypca(data);
%figure();
%plot(data(:,1), data(:,2), '*');
%title('Original data');

%figure();
%plot(Y(:,1), Y(:,2), '*');
%title('PCA data');
