function total_accuracy = judge_svm_parameters(pavia_trainset, pavia_testset, sigma, C)
  train_classes = pavia_trainset.nlab(pavia_trainset.nlab == 4 | pavia_trainset.nlab == 5);
  train_data = pavia_trainset.data(pavia_trainset.nlab == 4 | pavia_trainset.nlab == 5, :);
  
  test_classes = pavia_testset.nlab(pavia_testset.nlab == 4 | pavia_testset.nlab == 5);
  test_data = pavia_testset.data(pavia_testset.nlab == 4 | pavia_testset.nlab == 5, :);
  
  max_train = max(train_data);
  min_train = min(train_data);
  for i=1:size(train_data)
    train_data(i,:) = 2*(train_data(i,:) - min_train)./(max_train - min_train) - 1;
  end
  for i=1:size(test_data)
    test_data(i,:) = 2*(test_data(i,:) - min_train)./(max_train-min_train) - 1;
  end

  % 10-fold cross-validation with trainset
  accuracy = 0;
  setsize = round(size(train_data, 1)/10)
  for i = 0:9
    start = i*setsize + 1;
    stop = min((i+1)*setsize, size(train_data, 1));
    
    cv_testset_data = train_data(start:stop, :);
    cv_testset_classes = train_classes(start:stop, :);

    cv_trainset_data = [train_data(1:(start-1), :); train_data((stop+1):end, :)];
    cv_trainset_classes = [train_classes(1:(start-1), :); train_classes((stop+1):end, :)];

    accuracy = accuracy + svm_iteration(sigma, C, cv_trainset_classes, cv_trainset_data, cv_testset_classes, cv_testset_data);
  end
  total_accuracy = accuracy / 10;
end
