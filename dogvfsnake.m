function dogvfsnake(input_image, original, samples, iterations, GVF_iterations, weight_first_derivative, weight_second_derivative, weight_field, stiffness)
  [M, N] = size(original);
  epsi = 1e-10;

  % compute the GVF
  [u,v] = GVF(input_image, 0.2, GVF_iterations);
  
  % set up the ellipse
  major_axis = 0.15*sqrt(M*M + N*N);
  minor_axis = 0.20*sqrt(M*M + N*N);

  x0 = (major_axis*cos(0:(2*pi/samples):2*pi-2*pi/samples))' + N/2 - 10;
  y0 = (minor_axis*sin(0:(2*pi/samples):2*pi-2*pi/samples))' + M/2 - 10;

  r1 = zeros([samples,1]);
  r1(1) = -2*weight_first_derivative+6*weight_second_derivative;
  r1(2) = -weight_first_derivative-4*weight_second_derivative;
  r1(3) = weight_second_derivative;
  r1(end - 1) = weight_second_derivative;
  r1(end) = -weight_first_derivative-4*weight_second_derivative;

  B = toeplitz(r1) + stiffness*eye(samples);

  x = x0;
  y = y0;

  px = u./(sqrt(u.*u + v.*v) + epsi);
  py = v./(sqrt(u.*u + v.*v) + epsi);
  
  for i = 0:iterations
    xv = stiffness*x ...
	+ weight_field*px(sub2ind([M, N], round(y + 1), round(x + 1)));
    yv = stiffness*y ...
	+ weight_field*py(sub2ind([M, N], round(y + 1), round(x + 1)));
    x = B\xv;
    y = B\yv;
  end

  hold 'on'
  imagesc(original);
  colormap('gray');
  axis('image');
  
  plot([x0; x0(1)],[y0; y0(1)], 'b', 'Linewidth', 2);  
  plot([x;x(1)],[y;y(1)], 'r', 'Linewidth', 2);
  
  print('gvfsnake-result.png', '-dpng');
  hold 'off'
end